package com.example.ensai.enigmz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter{

    private List<String> donnees;
    private Context contexte;

    public MyAdapter(Context context, List<String> data){
        donnees=data;
        contexte=context;
    }


    @Override
    public int getCount() {
        return donnees.size();
    }

    @Override
    public String getItem(int position) {
        return donnees.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v;
        if (convertView==null){
            LayoutInflater inflater= (LayoutInflater) contexte.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inflater.inflate(R.layout.enigme,null);
        }
        else{
            v=convertView;
        }
        String element= getItem(position);
//        TextView textEnigme= (TextView) v.findViewById(R.id.textEnigme);
//        textEnigme.setText(element);
        return v;
    }

}