package com.example.ensai.enigmz;

/**
 * Created by ensai on 09/05/17.
 */

public class TypeEnigme {

    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public TypeEnigme(String nom){
        this.nom=nom;
    }

    @Override
    public String toString() {
        return this.nom;
    }
}
