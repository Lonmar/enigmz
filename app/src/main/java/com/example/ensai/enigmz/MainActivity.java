package com.example.ensai.enigmz;

import android.content.Intent;
import android.opengl.EGLExt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickJouer(View v){
        Intent i = new Intent(MainActivity.this,Definitions.class);
        startActivity(i);
    }

    public void clickRegle(View v){
        Intent i = new Intent(MainActivity.this,Regle.class);
        startActivity(i);
    }
}
