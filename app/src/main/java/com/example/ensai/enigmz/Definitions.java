package com.example.ensai.enigmz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Definitions extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_definitions);

        ListView listeDef = (ListView) findViewById(R.id.listeDefinitions);

        TypeEnigme[] types=new TypeEnigme[] {
                new TypeEnigme("Précieux"),
                new TypeEnigme("Constellations"),
                new TypeEnigme("Tableau Périodique"),
                new TypeEnigme("Monuments Historiques")
        };

        ArrayAdapter<TypeEnigme> adapter = new ArrayAdapter<TypeEnigme>(this, android.R.layout.simple_list_item_1,types);

        listeDef.setAdapter(adapter);

        listeDef.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = ((TextView)view).getText().toString();

                Toast.makeText(getBaseContext(),item,Toast.LENGTH_SHORT).show();

                Intent intent;

                switch(position) {
                    case 0:
                        intent = new Intent(getApplicationContext(), Precieux.class);
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(getApplicationContext(), Constellations.class);
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(getApplicationContext(), TableauPeriodique.class);
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(getApplicationContext(), Monuments.class);
                        startActivity(intent);
                        break;
                    default:
                }
            }
        });
    }
}
