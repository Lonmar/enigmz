package com.example.ensai.enigmz;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by ensai on 28/05/17.
 */

public class EnigmeConstellations extends AppCompatActivity {

    String laReponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enigme);

        // On récupère l'intent qui a lancé cette activité
        Intent i = getIntent();

        // Puis on récupère les infos dans l'autre activité
        //String motAffiche = i.getStringExtra(Precieux.motAffiche);
        laReponse = i.getStringExtra(Constellations.laReponse);
    }


    public void clickValider(View v){
        EditText reponseDuJoueur=(EditText) findViewById(R.id.editEnigm);
        //Toast.makeText(this,laReponse, Toast.LENGTH_LONG).show();
        SQLiteDatabase bdd=new BDD(this).getWritableDatabase();
//        Toast.makeText(this,laReponse, Toast.LENGTH_LONG).show();
//        Toast.makeText(this,reponseDuJoueur.getText().toString(), Toast.LENGTH_LONG).show();

        //n'entre pas dans le if. Pourquoi ?
        if (reponseDuJoueur.getText().toString().equalsIgnoreCase(laReponse)){
            MediaPlayer player= MediaPlayer.create(EnigmeConstellations.this,R.raw.bee);
            player.start();

            Toast.makeText(this,"Bonne réponse !", Toast.LENGTH_LONG).show();
            ContentValues values = new ContentValues();
            values.put("trouver", 1);
            bdd.update("bdd", values, "mot = ? AND categorie = 'Constellations' ",new String[] {laReponse} );
            //bdd.execSQL("UPDATE TABLE bdd SET trouver=1 WHERE categorie = 'Precieux' AND mot = ? ", new String[] {laReponse});
        }
        else{
            Vibrator vib = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            vib.vibrate(500);

            Toast.makeText(this,"Ce n'est pas la bonne réponse !", Toast.LENGTH_LONG).show();
        }

        bdd.close();
        finish();
    }

}
