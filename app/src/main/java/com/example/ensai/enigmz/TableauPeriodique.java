package com.example.ensai.enigmz;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class TableauPeriodique extends AppCompatActivity {

    //public final static String motAffiche = "com.example.ensai.enigmz.intent.motAffiche";
    public final static String laReponse = "com.example.ensai.enigmz.intent.laReponse";

    private TextView champPctTableauPeriodique;

    List<String> elements=new ArrayList<String>();
    List<String> reponsesEnigmes=new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tableau_periodique);
        ListView liste=(ListView) findViewById(R.id.listeTableauPeriodique);

        Pourcentage();

        chargerBDD();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elements);
        liste.setAdapter(adapter);

        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = ((TextView)view).getText().toString();
                String reponse = reponsesEnigmes.get(position);

                Toast.makeText(getBaseContext(),item,Toast.LENGTH_SHORT).show();

                Intent intent;
                intent = new Intent(getApplicationContext(), EnigmeTableauPeriodique.class);
                //intent.putExtra(Precieux.motAffiche, item);
                //intent.putExtra(Precieux.positionMot, position);
                intent.putExtra(TableauPeriodique.laReponse, reponse);
                startActivity(intent);

            }
        });

    }

    protected void onResume() {
        super.onResume();
        chargerBDD();
        Pourcentage();
        adapter.notifyDataSetChanged();
    }

    public void Pourcentage(){
        //ce qui concerne le champs du pourcentage d'enigme resolu
        champPctTableauPeriodique = (TextView) findViewById(R.id.champPctTableauPeriodique);
        int nbEnigmes=0;
        int nbEnigmesResolus=0;
        SQLiteDatabase bdd= new BDD(this).getReadableDatabase();
        Cursor cursor1 = bdd.rawQuery("SELECT count(*) FROM bdd WHERE categorie='TableauPeriodique'",null);
        Cursor cursor2 = bdd.rawQuery("SELECT count(*) FROM bdd WHERE categorie='TableauPeriodique' AND trouver=1",null);
        while (cursor1.moveToNext()){
            nbEnigmes=cursor1.getInt(0);
        }
        while (cursor2.moveToNext()){
            nbEnigmesResolus=cursor2.getInt(0);
        }
        cursor1.close();
        cursor2.close();
        bdd.close();
        champPctTableauPeriodique.setText(nbEnigmesResolus+"/"+nbEnigmes + " énigmes résolus");
    }

    public void clickBoutonResoluTableauPeriodique(View v){
        Toast.makeText(getBaseContext(),"Enigmes Résolus",Toast.LENGTH_SHORT).show();

        Intent intent1 = new Intent(TableauPeriodique.this, EnigmeResoluTableauPeriodique.class);
        startActivity(intent1);
    }

    public void chargerBDD(){
        reponsesEnigmes.clear();
        elements.clear();
        SQLiteDatabase bdd= new BDD(this).getReadableDatabase();
        Cursor cursor = bdd.rawQuery("SELECT mot FROM bdd WHERE categorie='TableauPeriodique' AND trouver = 0",null);
        while (cursor.moveToNext()){
            String element=cursor.getString(0);
            reponsesEnigmes.add(element);
            elements.add(element.substring(0,1) + " en " + element.length() + " lettres.");
        }
        cursor.close();
        bdd.close();

    }
}
