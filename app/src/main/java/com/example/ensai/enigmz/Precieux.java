package com.example.ensai.enigmz;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Precieux extends AppCompatActivity {

    //public final static String motAffiche = "com.example.ensai.enigmz.intent.motAffiche";
    //public final static String positionMot = "com.example.ensai.enigmz.intent.positionMot";
    public final static String laReponse = "com.example.ensai.enigmz.intent.laReponse";
    private TextView champPctPrecieux;

    List<String> elements=new ArrayList<String>();
    List<String> reponsesEnigmes=new ArrayList<String>();
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_precieux);
        ListView liste=(ListView) findViewById(R.id.listePrecieux);

        Pourcentage();

        chargerBDD();
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, elements);
        liste.setAdapter(adapter);

        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = ((TextView)view).getText().toString();
                String reponse = reponsesEnigmes.get(position);

                Toast.makeText(getBaseContext(),item,Toast.LENGTH_SHORT).show();

                Intent intent;
                intent = new Intent(getApplicationContext(), EnigmePrecieux.class);
                //intent.putExtra(Precieux.motAffiche, item);
                //intent.putExtra(Precieux.positionMot, position);
                intent.putExtra(Precieux.laReponse, reponse);
                startActivity(intent);

            }
        });

    }

    protected void onResume() {
        super.onResume();
        chargerBDD();
        Pourcentage();
        adapter.notifyDataSetChanged();
    }

    public void Pourcentage(){
        //ce qui concerne le champs du pourcentage d'enigme resolu
        champPctPrecieux = (TextView) findViewById(R.id.champPctPrecieux);
        int nbEnigmes=0;
        int nbEnigmesResolus=0;
        SQLiteDatabase bdd= new BDD(this).getReadableDatabase();
        Cursor cursor1 = bdd.rawQuery("SELECT count(*) FROM bdd WHERE categorie='Precieux'",null);
        Cursor cursor2 = bdd.rawQuery("SELECT count(*) FROM bdd WHERE categorie='Precieux' AND trouver=1",null);
        while (cursor1.moveToNext()){
            nbEnigmes=cursor1.getInt(0);
        }
        while (cursor2.moveToNext()){
            nbEnigmesResolus=cursor2.getInt(0);
        }
        cursor1.close();
        cursor2.close();
        bdd.close();
        champPctPrecieux.setText(nbEnigmesResolus+"/"+nbEnigmes + " énigmes résolus");
    }

    public void clickBoutonResoluPrecieux(View v){
        Toast.makeText(getBaseContext(),"Enigmes Résolus",Toast.LENGTH_SHORT).show();

        Intent intent1 = new Intent(Precieux.this, EnigmeResoluPrecieux.class);
        startActivity(intent1);
    }

    public void chargerBDD(){
        reponsesEnigmes.clear();
        elements.clear();
        SQLiteDatabase bdd= new BDD(this).getReadableDatabase();
        Cursor cursor = bdd.rawQuery("SELECT mot FROM bdd WHERE categorie='Precieux' AND trouver = 0",null);
        while (cursor.moveToNext()){
            String element=cursor.getString(0);
            reponsesEnigmes.add(element);
            elements.add(element.substring(0,1) + " en " + element.length() + " lettres.");
        }
        cursor.close();
        bdd.close();

//        MyAdapter adapter=new MyAdapter(this,elements);
//        ListView liste=(ListView) findViewById(R.id.listePrecieux);
//        liste.setAdapter(adapter);
    }
}
