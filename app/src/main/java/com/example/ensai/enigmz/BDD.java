package com.example.ensai.enigmz;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDD extends SQLiteOpenHelper {

    private static final int BDD_version=1;
    private static final String BDD_name="mabase";

    public BDD(Context context){
        super(context,BDD_name,null,BDD_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE bdd (categorie TEXT, mot TEXT, trouver boolean default 0 check(trouver in (0,1)))");
        //enigme catégorie Precieux
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','OR',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','ARGENT',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','PLATINE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','VERMEIL',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','DIAMANT',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','RUBIS',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','SAPHIR',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','EMERAUDE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','TOPAZE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','GRENAT',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','OPALE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','AGATE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','ALEXANDRITE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','AIGUE-MARINE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','AMETHYSTE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','JADE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','LAPIS-LAZULI',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','OBSIDIENNE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','ONYX',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','TOURMALINE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','TURQUOISE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','PERLE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','EBENE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','IVOIRE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','MARBRE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Precieux','AMBRE',0)");
        //enigme catégorie Constellations
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','POISSONS',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','CAPRICORNE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','BALANCE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','BELIER',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','VIERGE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','GEMEAUX',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','ORION',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','LION',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','CANCER',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','TAUREAU',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','SAGITTAIRE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','CASSIOPEE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','VERSEAU',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','CENTAURE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','HERCULE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Constellations','SCORPION',0)");
        //enigme catégorie Monuments
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','TOUR EIFFEL',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','PYRAMIDE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','JARDINS SUSPENDUS',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','TEMPLE D''ARTEMIS',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','MAUSOLEE D''HALICARNASSE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','COLOSSE DE RHODES',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','PHARE D''ALEXANDRIE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','STONEHENGE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','PARTHENON',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','TAJ MAHAL',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','COLISEE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('Monuments','MACHU PICCHU',0)");
        //enigme catégorie TableauPeriodique
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','URANIUM',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','PLUTONIUM',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','CURIUM',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','RADIUM',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','THORIUM',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','SELENIUM',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','MERCURE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','ARSENIC',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','AZOTE',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','LITHIUM',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','COBALT',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','NICKEL',0)");
        db.execSQL("INSERT INTO bdd VALUES ('TableauPeriodique','OXYGENE',0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
