package com.example.ensai.enigmz;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class EnigmeResoluMonuments extends AppCompatActivity {

    ArrayAdapter<String> adapter;
    List<String> enigmesResolus=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enigme_resolu_monuments);
        ListView liste=(ListView) findViewById(R.id.listeResoluMonuments);

        SQLiteDatabase bdd= new BDD(this).getReadableDatabase();
        Cursor cursor = bdd.rawQuery("SELECT mot FROM bdd WHERE categorie='Monuments' AND trouver = 1",null);
        while (cursor.moveToNext()){
            String element=cursor.getString(0);
            enigmesResolus.add(element);
        }
        cursor.close();
        bdd.close();

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, enigmesResolus);
        liste.setAdapter(adapter);

    }
}
